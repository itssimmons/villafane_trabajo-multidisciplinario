﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea004remake
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Cantidad de ventas: ");
			int num = int.Parse(Console.ReadLine());
			DatosVenta venta = new DatosVenta();
			//******//
			venta.IngresarDatos(num);
			//*******//
			venta.PromedioDeVentas(num);
			venta.VentaMaxima(num);
			venta.VentaMinima(num);
			venta.PromedioEmpleados(num);
			venta.VentaTotal(num);	
		}
	}
}
